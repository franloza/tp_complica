package tp.pr2;

import tp.pr2.control.*;
import tp.pr2.logic.*;
import java.util.Scanner;

/**
 * Class that contains the entry point to the application.
 * @author: �lvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 */

public class Main  {
	
	//Methods	
	 /**
     * Main method of the application
     * @param args Arguments passed to the application. Not used.
     */
	public static void main (String[] args ) {
		Connect4Rules c4rules = new Connect4Rules();
		Game game = new Game(c4rules);
		Scanner in = new Scanner (System.in);
		Controller control = new Controller (game, in);
		control.run();
	}
}
