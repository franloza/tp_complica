package tp.pr2.control;

import java.util.Scanner;
import tp.pr2.logic.*;

/**
 * Class which controls the execution of the game, presenting the user with a set of alternatives and asking which he/she 
 * wishes to choose until the game finishes.
 * @author: Álvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 1
 */

public class Controller {
	
	//Attributes
	private Game game;
	private Scanner in;
	private GameType playingTo;
	
	//Constructor
	/**
     * Class constructor.
     * @param p The game which is to be played
     * @param in The scanner to be used to read the information the user provides.
     */
	public Controller (Game p, Scanner in) {
		this.game = p;
		this.in = in;
		this.playingTo = GameType.CONNECT4;
	}
	
	//Methods
	/**
     * Execute the game until it finishes. 
     * It is assumed that this method is called just once. If it is called again, the behavior is undefined.
     */
	public void run() {
		boolean exit = false;
		Counter turn;
		ComplicaMove moveCompl = new ComplicaMove (0, Counter.EMPTY);
		Connect4Move moveC4 = new Connect4Move (0, Counter.EMPTY);
		String command;		
		
		while (!this.game.isFinished() && !exit) {
				System.out.println (this.game.getBoard().toString());
				turn = game.getTurn();						
				System.out.println (Util.strTurn(turn) + " to move");
				System.out.print ("Please enter a command: ");
				command = in.nextLine();
				command = command.toLowerCase(); //Converts the command to lower case
				command = command.replace(" ","");
				switch (command) {
					case ("makeamove"):
					{
						int column;
						boolean validColumn;
						System.out.print ("Please provide the column number: ");
						column = in.nextInt();
						in.nextLine();
						if (playingTo == GameType.CONNECT4) {
							moveC4 = new Connect4Move(column,game.getTurn());
							validColumn = game.executeMove(moveC4);
						}
						else {
							moveCompl = new ComplicaMove(column,game.getTurn());
							validColumn = game.executeMove(moveCompl);
						}
						
						if (!validColumn) {
								System.out.println ("Invalid move, please try again");
						}
						else if (this.game.isFinished()){
							System.out.println (this.game.getBoard().toString());
							System.out.println ("Game over. " + Util.strTurn(game.getWinner()) + " wins");
							System.out.print("Closing the game...");
						}
					}
					break;	
					case ("undo"):
					{
						boolean undo;
						undo = this.game.undo();
						if (!undo) { //Commented because validator
							System.err.println ("Nothing to undo, please try again");
						}						
					}
					break;	
					case ("restart"):
					{
						this.game.reset(playingTo.getGameRules());
						System.out.print ("Game restarted");
					}
					break;	
					case ("exit"):
					{
						exit = true;
						System.out.println ("Closing the game... ");
						
					}
					break;
					case ("playc4"):
					{
						this.game = new Game(GameType.CONNECT4.getGameRules());
						this.playingTo = GameType.CONNECT4;
						System.out.println ("Game restarted");
					}
					break;
						
					case ("playco"):
					{
						this.game = new Game(GameType.COMPLICA.getGameRules());
						this.playingTo = GameType.COMPLICA;							
						System.out.println ("Game restarted");
					}
					break;					
					default: {
						System.err.println (command + ": command not understood, please try again");
					}
			}	
		}
	}

}
