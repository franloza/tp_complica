package tp.pr2.logic;

/**
 * Class that implements the move for the Complica game, implementing the abstract methods of the parent class.
 * @author: �lvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 2
 * @see: tp.pr2.logic.Move
 */
public class ComplicaMove extends Move {
	
	//Attributes
	private Counter bottom;
	
	//Constructor
	/**
     * Class constructor. Invokes the constructor of the superclass 
     * @param moveColumn Column in which to place the counter.
     * @param moveColour Colour of the counter to be placed (also that of the player that places it).
     */
	public ComplicaMove(int moveColumn, Counter moveColour){
		super(moveColumn,moveColour);
		bottom = Counter.EMPTY;
	}

	//Methods (Defined in the superclass Move)
	
	@Override
	public boolean executeMove(Board board) {
		boolean valid;
		if (getMoveColumn() >= 1 && getMoveColumn() <= board.getWidth()) {
			valid = true;
			if (Util.topCounter(board, getMoveColumn()) > 1) {  //The column is not full and set the counter
				board.setPosition(getMoveColumn(), Util.topCounter(board, getMoveColumn()) - 1, getMoveColour());
			}
			else { //The column is full and push down the column and set the new counter
				bottom = Util.pushColumn (board, getMoveColumn(),getMoveColour());
			}
		}
		else {
			valid = false;
		}

		return valid;		
	}
	
	@Override
	public void undo(Board board) {
		if (bottom == Counter.EMPTY) { //The column wasn't push down in this move
			int undo_row = Util.topCounter(board,getMoveColumn());
			board.setPosition(getMoveColumn(), undo_row, Counter.EMPTY);
		}
		else {
			Util.pullColumn(board,getMoveColumn(),bottom);
		}
	}	
}