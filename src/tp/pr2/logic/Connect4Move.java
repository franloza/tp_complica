package tp.pr2.logic;

/**
 * Class that implements the move for the Connect-4 game, implementing the abstract methods of the parent class.
 * @author: �lvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 2
 * @see: tp.pr2.logic.Move
 */

public class Connect4Move extends Move {
		//Constructor
		/**
	     * Class constructor. Invokes the constructor of the superclass 
	     * @param moveColumn Column in which to place the counter.
	     * @param moveColour Colour of the counter to be placed (also that of the player that places it).
	     */
		public Connect4Move(int moveColumn, Counter moveColour){
			super(moveColumn, moveColour);
		}
		
		//Methods (Defined in the superclass Move)
		public boolean executeMove(Board board){
			boolean valid;
			if (getMoveColumn() >= 1 && getMoveColumn() <= board.getWidth() 
					&& Util.topCounter(board,getMoveColumn()) > 1) {
				valid = true;
				board.setPosition(getMoveColumn(), Util.topCounter(board,getMoveColumn()) - 1, getMoveColour());			}
			else
				valid = false;
			return valid;		
		}
		
		public void undo(Board board){	
			int undo_row = Util.topCounter(board,getMoveColumn());
			board.setPosition(getMoveColumn(), undo_row, Counter.EMPTY);						
		}

}

