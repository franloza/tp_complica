package tp.pr2.logic;

/**
 * Class that implements the rules for the Connect-4 game, implementing all the methods of the interface, as well as the constructor.
 * @author: �lvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 2
 * @see: tp.pr2.logic.GameRules
 */


public class Connect4Rules implements GameRules{

	//Constants
	private static final byte WIDTH = 7;
	private static final byte HEIGHT = 6;
	
	//Methods (Defined in the interface class GameRules)
	public Counter initialPlayer() {
		return Counter.WHITE;
	}

	public boolean isDraw(Counter lastPlayer, Board board) {
		boolean draw = false;
		if (Util.checkFull(board) && !Util.check4(board)) {
			draw = true;
		}	
		return draw;
	}
	
	public Board newBoard() {
		Board board = new Board(WIDTH,HEIGHT);
		return board;
	}

	public Counter nextTurn(Counter lastPlayer, Board board) { 
		return Util.changeTurn(lastPlayer);
	}
	
	public Counter winningMove(Move lastMove, Board board) {
		Counter winningMove = Counter.EMPTY;
		if(Util.check4(board)) {
			winningMove = lastMove.getMoveColour();
		}
		return winningMove;
	}

}
