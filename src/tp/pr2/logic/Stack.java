package tp.pr2.logic;

/**
 * Class that creates and manages a stack composed of Move objects.
 * This class is used to save a record of movements to carry out undo actions in the game
 * @author: �lvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 2
 */

public class Stack {		
		
		//Constructor
		/**
	     * Constructs a stack of Move objects of a given dimension and sets the counter to 0
	     * @param dim Dimension of the stack
	     */
		public Stack (int dim) {
			this.stackDim = dim;
			this.undoStack = new Move[stackDim];
			for (int i = 0; i < stackDim; ++i)
				this.undoStack[i] = null; 
			this.numUndo = 0;
			
		}
		
		//Methods
		/**
	     * Method which returns the last element of the stack and decreases the counter of the stack if the stack is not empty.
	     * If it is empty, returns a null element.
	     * @return The last element of the stack
	     */
		public Move pop () { 
			Move lastElement;
			if (numUndo == 0) {
				lastElement = null; 
			}
			else {			
				lastElement = this.undoStack[this.numUndo - 1];

				--this.numUndo;
			}
			return lastElement;
		}
		
		/**
	     * Method which inserts a new movement in the Stack. 
	     * If the Stack is full,it erases the first movement and shifts the other movements. 
	     * @param element The movement to insert in the Stack
	     */
		public void push (Move element) { 
			if(numUndo < stackDim) {
				undoStack[numUndo] = element;
				++numUndo;
			}
			else { //This allows to renew the array when it's full one by one
				for (int i = 0; i < stackDim - 1; i++) {
					undoStack[i] = undoStack[i+1];						
				}		
				undoStack[stackDim - 1] = element;
			}
		}
		
		/**
	     * Method which checks if the Stack is empty or not
	     * @return True if the Stack is empty
	     */
		public boolean is_empty () {
			boolean is_void = false;
			if (numUndo == 0)
				is_void = true;
			return is_void;
		}
		
		//Atributes
		private Move[] undoStack;
		private int numUndo;
		private int stackDim;

}
