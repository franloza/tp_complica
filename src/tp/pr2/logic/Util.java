package tp.pr2.logic;

/**
 * Class which collects several static methods that serve as utilities in the rest of the classes of the program.
 * @author: �lvaro Bermejo 
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 2
 */

public class Util {
	
	//Methods
	/**
     * Method which returns the upper counter of a given column that is not empty. If the column is full, it returns 1
     * @return The corresponding row with the upper non-empty counter of a column
     */
	public static int topCounter (Board board,int column) {
		int row = 1;
		while (row <= board.getHeight() && (board.getPosition(column, row) ==  Counter.EMPTY)) {
			++row;			
		}
		
		return row; //row = 1 means the column it's full
	}
	
	/**
     * Method which checks if there is a 4 counter combination in a given board according to the Connect 4 rules.
     * @param board The board of the game
     * @return True if there is a 4 counter combination either vertical, horizontal or diagonal
     */
	public static boolean check4 (Board board) {
		int i = 0, j = 0;
		boolean finished = false;
		Counter firstTile;
		//Horizontal check
		while (i < board.getHeight() && !finished) {
			while (j < board.getWidth() - 3 && !finished) {
				firstTile = board.getPosition(j+1, i+1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(j+2, i+1) ) {
						if (firstTile == board.getPosition(j+3, i+1) ) {
							if (firstTile == board.getPosition(j+4, i+1) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}	
		
		//Vertical check
		i = 0; j = 0;
		while (i < board.getWidth() && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i+1, j+1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(i+1, j+2) ) {
						if (firstTile == board.getPosition(i+1, j+3) ) {
							if (firstTile == board.getPosition(i+1, j+4) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}
		
		
		//Diagonal check
		i = 0; j = 0;
		while (i < board.getWidth() -3 && !finished) {
			while (j < board.getHeight()-3 && !finished) {
				firstTile = board.getPosition(i+1, j+1);
				if (firstTile != Counter.EMPTY) {	
					if (firstTile == board.getPosition(i+2,j+2) ) {
						if (firstTile == board.getPosition(i+3,j+3) ) {
							if (firstTile == board.getPosition(i+4,j+4) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}	
		
		//Inverted Diagonal check
		i = 3; j = 0;
		while (i < board.getWidth() && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i+1, j+1);
				if (firstTile != Counter.EMPTY) {	
					if (firstTile == board.getPosition(i,j+2) ) {
						if (firstTile == board.getPosition(i-1,j+3) ) {
							if (firstTile == board.getPosition(i-2,j+4) ) {	
								finished = true;
							}		
						}												
					}
				}
				j++;
			}			
			i++;
			j=0;
		}	
		
		return finished;
	}
	
	/**
     * Method which checks if there is a 4 counter combination in a given board according to the Complica rules.
     * @param board The board of the game
     * @return True if there is a 4 counter combination either vertical, horizontal or diagonal
     */
	public static Counter complicaCheck4 (Board board) {
		int i = 0, j = 0;
		boolean finished = false;
		Counter firstTile;

		Counter player4 = Counter.EMPTY;
		//Horizontal check
		while (i < board.getHeight() && !finished) {
			while (j < board.getWidth() - 3 && !finished) {
				firstTile = board.getPosition(j + 1, i + 1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(j + 2, i + 1)) {
						if (firstTile == board.getPosition(j + 3, i + 1)) {
							if (firstTile == board.getPosition(j + 4, i + 1)) {
								if (board.getPosition(j + 4, i + 1) != player4 && player4 != Counter.EMPTY) {
									finished = true;
									player4 = Counter.EMPTY;
								}
								else {
									player4 = board.getPosition(j + 4, i + 1);
								}
							}
						}
					}
				}
				j++;
			}
			i++;
			j = 0;
		}

		//Vertical check
		i = 0;
		j = 0;
		while (i < board.getWidth() && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i + 1, j + 1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(i + 1, j + 2)) {
						if (firstTile == board.getPosition(i + 1, j + 3)) {
							if (firstTile == board.getPosition(i + 1, j + 4)) {
								if (board.getPosition(i + 1, j + 4) != player4 && player4 != Counter.EMPTY) {
									finished = true;
									player4 = Counter.EMPTY;
								}
								else {
									player4 = board.getPosition(i + 1, j + 4);
								}
							}
						}
					}
				}
				j++;
			}
			i++;
			j = 0;
		}


		//Diagonal check
		i = 0;
		j = 0;
		while (i < board.getWidth() - 3 && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i + 1, j + 1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(i + 2, j + 2)) {
						if (firstTile == board.getPosition(i + 3, j + 3)) {
							if (firstTile == board.getPosition(i + 4, j + 4)) {
								if (board.getPosition(i + 4, j + 4) != player4 && player4 != Counter.EMPTY) {
									finished = true;
									player4 = Counter.EMPTY;
								}
								else {
									player4 = board.getPosition(i + 4, j + 4);
								}
							}
						}
					}
				}
				j++;
			}
			i++;
			j = 0;
		}

		//Inverted Diagonal check
		i = 3; j = 0;
		while (i < board.getWidth() && !finished) {
			while (j < board.getHeight() - 3 && !finished) {
				firstTile = board.getPosition(i + 1, j + 1);
				if (firstTile != Counter.EMPTY) {
					if (firstTile == board.getPosition(i, j + 2)) {
						if (firstTile == board.getPosition(i - 1, j + 3)) {
							if (firstTile == board.getPosition(i - 2, j + 4)) {
								if (board.getPosition(i - 2, j + 4) != player4 && player4 != Counter.EMPTY) {
									finished = true;
									player4 = Counter.EMPTY;
								}
								else {
									player4 = board.getPosition(i - 2, j + 4);
								}
							}
						}
					}
				}
				j++;
			}
			i++;
			j = 0;
		}

		return player4;
	}

	/**
     * Method which checks if a given board is full of counters
     * @param board The board of the game
     * @return True if the board is full
     */
	public static boolean checkFull (Board board) {
		boolean is_full = false;
		int i = 1;
		while (Util.topCounter(board,i) == 1 && i <= board.getWidth()) {
			++i;
		}
		if (i > board.getWidth()) {
			is_full = true;
		}	
		return is_full;
	}
	
	/**
     * Method that returns the inverse counter of another one (Either Black or White)
     * @param turn Counter to be changed
     * @return Inverse of the counter
     */
	public static Counter changeTurn (Counter turn) {
		Counter newTurn;
		if(turn == Counter.WHITE) {
			newTurn = Counter.BLACK;
		}
		else {
			newTurn = Counter.WHITE;
		}
		return newTurn;
	}
	
	/**
     * Method that insert a counter on the bottom of a column and moves up the rest of the counters of the column.
     * @param board Board to be changed
     * @param moveColumn Number of the column in which the counter will be inserted
     * @param counter Counter to be inserted
     * @return The top counter of the board that has been overwritten
     */
	public static Counter pushColumn(Board board, int moveColumn, Counter counter) {
		Counter bottom;
		bottom = board.getPosition(moveColumn, board.getHeight());
		//Copy the counter above to each counter starting from the bottom
		for (int i = board.getHeight(); i > 1; i-- ) {
			board.setPosition(moveColumn, i, board.getPosition(moveColumn, i-1));
		}
		board.setPosition(moveColumn,1,counter);	
		return bottom;
	}
	
	/**
     * Method that insert a counter on the top of a column and moves down the rest of the counters of the column.
     * @param board Board to be changed
     * @param moveColumn Number of the column in which the counter will be inserted
     * @param counter Counter to be inserted
     * @return The bottom counter of the board that has been overwritten
     */
	public static void pullColumn(Board board, int moveColumn, Counter counter) {
		//Copy the counter below to each counter starting from the top
		for (int i = 1; i < board.getHeight(); i++ ) {
			board.setPosition(moveColumn, i, board.getPosition(moveColumn, i+1));
		}
		board.setPosition(moveColumn,board.getHeight(),counter);		
	}
	
	/**
     * Method which converts a Counter to a String to be displayed on the console. (Either White or Black)
     * @param turn Counter to be converted
     * @return Text representation of the Counter
     */
	public static String strTurn (Counter turn) {
		String turnStr;
		if (turn == Counter.WHITE){
			turnStr = "White";
		}
		else {
			turnStr = "Black";
		}
		return turnStr;
	}
}


